#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>

#include "allocator.h"
#include "block.h"
#include "config.h"
#include "kernel.h"

static struct block *arena = NULL;

#define ARENA_SIZE (ALLOCATOR_ARENA_PAGES * ALLOCATOR_PAGE_SIZE)
#define BLOCK_SIZE_MAX (ARENA_SIZE - BLOCK_STRUCT_SIZE)

static int arena_alloc(void)
{
    arena = kernel_alloc(ARENA_SIZE);

    if (arena != NULL)
    {
        arena_init(arena, BLOCK_SIZE_MAX);
        return 0;
    }
    return -1;
}

void *mem_alloc(size_t size)
{
    struct block *block;

    if (arena == NULL)
        if (arena_alloc() < 0)
            return NULL;

    if (size >= BLOCK_SIZE_MAX)
        return NULL;

    size = ROUND_BYTES(size);

    for (block = arena;; block = block_next(block))
    {
        if (!block_get_flag_busy(block) && block_get_size_curr(block) >= size)
        {
            block_split(block, size);
            return block_to_payload(block);
        }

        if (block_get_flag_last(block))
            break;
    }

    return NULL;
}

void mem_free(void *ptr)
{
    struct block *block, *block_r, *block_l;

    if (ptr == NULL)
        return;

    block = payload_to_block(ptr);
    block_clr_flag_busy(block);

    if (!block_get_flag_last(block))
    {
        block_r = block_next(block);

        if (!block_get_flag_busy(block_r))
            block_merge(block, block_r);
    }

    if (!block_get_flag_first(block))
    {
        block_l = block_prev(block);

        if (!block_get_flag_busy(block_l))
            block_merge(block_l, block);
    }


}

void *mem_realloc(void *ptr, size_t size)
{
    if (ptr == NULL)
    {
        return mem_alloc(size);
    }

    if (size == 0)
    {
        mem_free(ptr);
        return NULL;
    }

    if (size > BLOCK_SIZE_MAX)
        return NULL;

    if (size < BLOCK_SIZE_MIN)
        size = BLOCK_SIZE_MIN;

    size = ROUND_BYTES(size);

    struct block *block = payload_to_block(ptr);

    if (size == block_get_size_curr(block))
    {
        return ptr;
    }

    else if(size < block_get_size_curr(block)){
        size_t size_rest = block_get_size_curr(block) - size;

        block_split(block, size);

        struct block *block_r = block_next(block);
        struct block *block_r_r = block_next(block_r);

        if(!block_get_flag_busy(block_r_r)){
            block_merge(block_r, block_r_r);
        }

        return block_to_payload(block);
    }

    else
    {
        size_t size_needed = size - block_get_size_curr(block);
        struct block *block_r = block_next(block);

        if (!block_get_flag_last(block) && !block_get_flag_busy(block_r) && block_get_size_curr(block_r) >= size_needed+BLOCK_STRUCT_SIZE)
        {
            block_merge(block, block_r);
            block_split(block, size);

            return block_to_payload(block);
        }
        else
        {
            void *new_ptr = mem_alloc(size);

            if (new_ptr != NULL)
            {
                memcpy(new_ptr, ptr, block_get_size_curr(block));
                mem_free(ptr);

                return new_ptr;
            }

            return NULL;
        }
    }
}

void mem_show()
{
    const struct block *block;

    printf("---------------------------------------------------------------------------------------------\n");
    printf(" %-16s| %-17s| %-15s| %-15s| %-5s| %-6s| %-5s|\n",  "Address","Payload", "Size", "Prev Size", "Busy", "First", "Last");
    printf("---------------------------------------------------------------------------------------------\n");

    if (arena == NULL)
    {
        printf("Arena was not created\n");
        return;
    }
    for (block = arena;; block = block_next(block))
    {
        printf("[%14p] | [%14p] | %-15zu| %-15zu| %-5s| %-6s| %-5s|\n",
               (void *)block,
               (void *)block_to_payload(block),
               block_get_size_curr(block), block_get_size_prev(block),
               block_get_flag_busy(block) ? "Yes" : "No",
               block_get_flag_first(block) ? "Yes" : "No",
               block_get_flag_last(block) ? "Yes" : "No");

        if (block_get_flag_last(block))
            break;
    }
    printf("---------------------------------------------------------------------------------------------\n");
}

bool is_valid_pointer(void *ptr) {
    const struct block *block;

    if (arena == NULL || ptr == NULL)
        return false;

    for (block = arena;; block = block_next(block)) {
        if (block == payload_to_block(ptr)) {
            return true;
        }

        if (block_get_flag_last(block)) {
            break;
        }
    }

    return false;
}
