#include <stdbool.h>

#include "allocator_impl.h"

struct block
{
    size_t size_curr;   // Size of the current block
    size_t size_prev;   // Size of the previous block
    bool flag_busy;     // Flag indicating whether the block is currently allocated
    bool flag_first;    // Flag indicating whether the block is the first in the arena
    bool flag_last;     // Flag indicating whether the block is the last in the arena
};

// Macro to calculate the size of block struct
#define BLOCK_STRUCT_SIZE ROUND_BYTES(sizeof(struct block))
#define BLOCK_SIZE_MIN ROUND_BYTES(sizeof(struct block))

void block_init(struct block *block);
void arena_init(struct block *block, size_t size);
void block_split(struct block *, size_t);
void block_merge(struct block *, struct block *);

// Function to convert block pointer to payload pointer
static inline void *block_to_payload(const struct block *block)
{
    return (char *)block + BLOCK_STRUCT_SIZE;
}

// Function to convert payload pointer to block pointer
static inline struct block *payload_to_block(const void *ptr)
{
    return (struct block *)((char *)ptr - BLOCK_STRUCT_SIZE);
}

// Getter and setter for current size of a block
static inline void block_set_size_curr(struct block *block, size_t size)
{
    block->size_curr = size;
}

static inline size_t block_get_size_curr(const struct block *block)
{
    return block->size_curr;
}

// Getter and setter for previous size of a block
static inline void block_set_size_prev(struct block *block, size_t size)
{
    block->size_prev = size;
}

static inline size_t block_get_size_prev(const struct block *block)
{
    return block->size_prev;
}

// Getter and setter for busy flag of a block
static inline void block_set_flag_busy(struct block *block)
{
    block->flag_busy = true;
}

static inline bool block_get_flag_busy(const struct block *block)
{
    return block->flag_busy;
}

// Function for clearing busy flag of a block
static inline void block_clr_flag_busy(struct block *block)
{
    block->flag_busy = false;
}

// Getter and setter for first flag of a block
static inline void block_set_flag_first(struct block *block)
{
    block->flag_first = true;
}

static inline bool block_get_flag_first(const struct block *block)
{
    return block->flag_first;
}

// Getter and setter for last flag of a block
static inline void block_set_flag_last(struct block *block)
{
    block->flag_last = true;
}

static inline bool block_get_flag_last(const struct block *block)
{
    return block->flag_last;
}

// Function for clearing last flag of a block
static inline void block_clr_flag_last(struct block *block)
{
    block->flag_last = false;
}

// Functions for getting next and previous blocks
static inline struct block *block_next(const struct block *block)
{
    // Returning the pointer to the next block
    return (struct block *)
           ((char *)block + BLOCK_STRUCT_SIZE + block_get_size_curr(block));
}

static inline struct block *block_prev(const struct block *block)
{
    // Returning the pointer to the previous block
    return (struct block *)
           ((char *)block - BLOCK_STRUCT_SIZE - block_get_size_prev(block));
}

