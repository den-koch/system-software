#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "allocator.h"
#include "tester.h"
#include "block.h"

static void *buf_alloc(size_t size)
{
    char *buf;
    size_t i;

    buf = mem_alloc(size);
    if (buf != NULL)
        for (i = 0; i < size; ++i)
            buf[i] = (char)rand();
    return buf;
}

int main(void)
{

    void *ptr1, *ptr2, *ptr3;

    buf_alloc(SIZE_MAX);
    mem_show();

    char cmd[100];
    size_t size;
    void *ptr;

    while (1)
    {
        printf("\nEnter command (alloc, free, realloc, show, exit): ");
        fgets(cmd, sizeof(cmd), stdin);

        cmd[strcspn(cmd, "\n")] = '\0';

        if (strcmp(cmd, "exit") == 0)
        {
            break;

        }
        else if (strcmp(cmd, "alloc") == 0)
        {
            printf("Enter size to allocate: ");
            scanf("%zu", &size);
            getchar();

            ptr = mem_alloc(size);

            if (ptr != NULL)
            {
                printf("Memory allocated at address: [%p]\n", payload_to_block(ptr));

            }
            else
            {
                printf("Failed to allocate memory!\n");
            }
        }
        else if (strcmp(cmd, "free") == 0)
        {
            printf("Enter payload to free: ");
            scanf("%p", &ptr);
            getchar();

            if (is_valid_pointer(ptr))
            {
                mem_free(ptr);
                printf("Memory freed!\n");
            }
            else
            {
                printf("No such payload!\n");
            }
        }
        else if (strcmp(cmd, "realloc") == 0)
        {
            printf("Enter payload to reallocate: ");
            scanf("%p", &ptr);
            getchar();

            if (is_valid_pointer(ptr))
            {
                printf("Enter new size: ");
                scanf("%zu", &size);
                getchar();

                void *new_ptr = mem_realloc(ptr, size);
                if (new_ptr != NULL)
                {
                    printf("Memory reallocated at new address: [%p]\n", payload_to_block(new_ptr));
                }
                else
                {
                    printf("Failed to reallocate memory!\n");
                }
            }
            else
            {
                printf("No such payload!\n");
            }
        }
        else if (strcmp(cmd, "show") == 0)
        {
            mem_show();
        }
        else
        {
            printf("Invalid command!!!\n");
        }
    }

//    tester(true);

    return 0;
}
