#include "kernel.h"
#include "config.h"

Kernel::Kernel()
{
    physicalPages.assign(NUM_PHYSICAL_PAGES, PhysicalPage());
    logFile.open("memory_report.txt");
}

Kernel::~Kernel()
{
    logFile.close();
}

void Kernel::createProcess()
{
    if (processes.size() >= MAX_PROCESSES)
    {
        logFile << "Error: Maximum number of processes reached\n";
        return;
    }

    Process process;
    process.processId = processes.size();
    process.pageTable.pagesPtr.assign(NUM_VIRTUAL_PAGES, -1);
    processes.push_back(process);
}

void Kernel::terminateProcess(int processId)
{
    if (processId < 0 || processId >= processes.size())
    {

        logFile << "Error: Invalid process ID\n";
        return;
    }

    for (int virtualPageNum : processes[processId].pageTable.pagesPtr)
    {
        if (virtualPageNum != -1)
        {
            physicalPages[virtualPageNum].occupied = false;
            physicalPages[virtualPageNum].processId = -1;
            physicalPages[virtualPageNum].virtualPageNum = -1;
            physicalPages[virtualPageNum].referenceBit = false;
            physicalPages[virtualPageNum].modifiedBit = false;
        }
    }

    processes.erase(processes.begin() + processId);
}

void Kernel::runProcess(int processId)
{
    if (processId < 0 || processId >= processes.size())
    {
        logFile << "Error: Invalid process ID\n";
        return;
    }

    Process &process = processes[processId];

    for (int i = 0; i < NUM_VIRTUAL_PAGES; i++)
    {
        int virtualPageNum = rand() % process.pageTable.pagesPtr.size();
        int virtualAddress = virtualPageNum * PAGE_SIZE;

        if (process.pageTable.pagesPtr[virtualPageNum] == -1)
            handlePageFault(processId, virtualPageNum);

        logFile << "Process " << processId << " accessed virtual address " << virtualAddress << endl;
        process.workingSet.virtualPageNums.insert(virtualPageNum);

        int physicalPageNum = process.pageTable.pagesPtr[virtualPageNum];
        physicalPages[physicalPageNum].referenceBit = true;

        if (rand() % 2)
        {
            modifyPage(processId, physicalPageNum);
        }
    }
}

void Kernel::modifyPage(int processId, int physicalPageNum)
{
    physicalPages[physicalPageNum].modifiedBit = true;
    logFile << "Process " << processId << " modified page " << physicalPageNum << endl;
}

void Kernel::handlePageFault(int processId, int virtualPageNum)
{
    Process &process = processes[processId];

    int freePhysicalPage = -1;
    for (int i = 0; i < NUM_PHYSICAL_PAGES; i++)
    {
        if (!physicalPages[i].occupied)
        {
            freePhysicalPage = i;
            break;
        }
    }

    if (freePhysicalPage == -1)
    {
        int victimPage;
        if (!useClock)
        {
            random_device rd;
            mt19937 gen(rd());
            uniform_int_distribution<int> dis(0, NUM_PHYSICAL_PAGES - 1);
            victimPage = dis(gen);
        }
        else
        {
            while (true)
            {
                if (!physicalPages[clockHand].referenceBit)
                {
                    victimPage = clockHand;
                    break;
                }
                if (physicalPages[clockHand].referenceBit)
                {
                    physicalPages[clockHand].referenceBit = false;
                }
                clockHand = (clockHand + 1) % NUM_PHYSICAL_PAGES;
            }
        }

        int oldVirtualPageNum = physicalPages[victimPage].virtualPageNum;
        processes[physicalPages[victimPage].processId].pageTable.pagesPtr[oldVirtualPageNum] = -1;

        process.pageTable.pagesPtr[virtualPageNum] = victimPage;
        physicalPages[victimPage].occupied = true;
        physicalPages[victimPage].processId = processId;
        physicalPages[victimPage].virtualPageNum = virtualPageNum;
        physicalPages[victimPage].referenceBit = true;
        physicalPages[victimPage].modifiedBit = false;

        logFile << "Process " << processId << " page fault (victim: " << victimPage << ")\n";
    }
    else
    {
        process.pageTable.pagesPtr[virtualPageNum] = freePhysicalPage;
        physicalPages[freePhysicalPage].occupied = true;
        physicalPages[freePhysicalPage].processId = processId;
        physicalPages[freePhysicalPage].virtualPageNum = virtualPageNum;
        physicalPages[freePhysicalPage].referenceBit = true;
        physicalPages[freePhysicalPage].modifiedBit = false;
        logFile << "Process " << processId << " page fault (new page: " << freePhysicalPage << ")\n";
    }

    process.pageFaults++;
    clockHand = (clockHand + 1) % NUM_PHYSICAL_PAGES;
}
