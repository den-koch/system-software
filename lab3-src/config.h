const int PAGE_SIZE = 4096;
const int NUM_PHYSICAL_PAGES = 8;
const int NUM_VIRTUAL_PAGES = 10;
const int MAX_PROCESSES = 4;
const bool useClock = true;