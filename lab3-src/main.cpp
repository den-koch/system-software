#include "config.h"
#include "kernel.h"

int main()
{
    Kernel kernel;

    for (int i = 0; i < MAX_PROCESSES+1; i++)
        kernel.createProcess();

    for (int i = 0; i < MAX_PROCESSES; i++)
    {
        kernel.logFile << "\nProcess call " << i << endl;
        int processId = i;
        kernel.runProcess(processId);
    }

    for (const Process &process : kernel.processes)
    {
        kernel.logFile << "\nProcess " << process.processId << endl;
        kernel.logFile << "Page faults: " << process.pageFaults << endl;
    }

    return 0;
}