#include <iostream>
#include <vector>
#include <random>
#include <set>
#include <ctime>
#include <fstream>

using namespace std;

struct PhysicalPage
{
    int processId = -1;
    bool occupied = false;
    bool referenceBit = false;
    bool modifiedBit = false;
    int virtualPageNum = -1;
};

struct PageTable
{
    vector<int> pagesPtr;
};

struct WorkingSet
{
    set<int> virtualPageNums;
};

struct Process
{
    int processId;
    PageTable pageTable;
    WorkingSet workingSet;
    int pageFaults = 0;
};

class Kernel {
public:
    std::vector<PhysicalPage> physicalPages;
    std::vector<Process> processes;
    int clockHand = 0;
    std::ofstream logFile;

    Kernel();
    ~Kernel();

    void createProcess();
    void terminateProcess(int processId);
    void runProcess(int processId);
    void modifyPage(int processId, int physicalPageNum);

private:
    void handlePageFault(int processId, int virtualPageNum);
};
