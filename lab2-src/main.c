#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "slab.h"
#include "tester.h"

int main()
{
    char cmd[100];
    size_t size;
    void *ptr;

    while (1)
    {
        printf("\nEnter command (alloc, free, realloc, show, exit): ");
        fgets(cmd, sizeof(cmd), stdin);

        cmd[strcspn(cmd, "\n")] = '\0';

        if (strcmp(cmd, "exit") == 0)
        {
            break;
        }
        else if (strcmp(cmd, "alloc") == 0)
        {
            printf("Enter size to allocate: ");
            scanf("%zu", &size);
            getchar();

            ptr = mem_alloc(size);

            if (ptr != NULL)
            {
                printf("Memory allocated at address: [%p]\n", ptr);
            }
            else
            {
                printf("Failed to allocate memory!\n");
            }
        }
        else if (strcmp(cmd, "free") == 0)
        {
            printf("Enter payload to free: ");
            scanf("%p", &ptr);
            getchar();

            mem_free(ptr);
            printf("Memory freed!\n");
        }
        else if (strcmp(cmd, "realloc") == 0)
        {
            printf("Enter payload to reallocate: ");
            scanf("%p", &ptr);
            getchar();

            printf("Enter new size: ");
            scanf("%zu", &size);
            getchar();

            void *new_ptr = mem_realloc(ptr, size);
            if (new_ptr != NULL)
            {
                printf("Memory reallocated at new address: [%p]\n", new_ptr);
            }
            else
            {
                printf("Failed to reallocate memory!\n");
            }
        }
        else if (strcmp(cmd, "show") == 0)
        {
            mem_show("123");
        }
        else if (strcmp(cmd, "tester") == 0)
        {
            tester(true);
        }
        else
        {
            printf("Invalid command!!!\n");
        }
    }

    return 0;
}
