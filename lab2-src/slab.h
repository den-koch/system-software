#include <stddef.h>

void *mem_alloc(size_t);
void mem_free(void *);
void mem_show(const char *);
void *mem_realloc(void *, size_t);

#define CACHE_SIZE 16
#define PAGE_SIZE (1 << 12)
#define PAGE_NUM 4096
#define MAX_OBJ_SIZE PAGE_SIZE
