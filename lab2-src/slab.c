#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "kernel.h"
#include "slab.h"

#define ALIGN _Alignof(max_align_t)
#define ROUND(x, y) (((x) + ((y) - 1)) & ~((y) - 1))
#define ROUND_BYTES(x) ROUND((x), ALIGN)

typedef struct slab_header
{
    size_t size;
    size_t used;
    void *free;
} slab_header_t;

typedef struct slab
{
    slab_header_t header;
    char data[PAGE_NUM * PAGE_SIZE];
} slab_t;

typedef struct cache
{
    slab_t *slabs[CACHE_SIZE];
    size_t count;
} cache_t;

cache_t cache;

slab_t *alloc_slab(size_t size)
{
    slab_t *slab = kernel_alloc(sizeof(slab_t));
    if (!slab)
        return NULL;

    slab->header.size = size;
    slab->header.used = 0;
    slab->header.free = slab->data;

    return slab;
}

void free_slab(slab_t *slab)
{
    kernel_free(slab);
}

void *mem_alloc(size_t size)
{
    size = ROUND_BYTES(size);

    if (size > MAX_OBJ_SIZE)
    {
        return NULL;
    }

    for (int i = 0; i < cache.count; i++)
    {
        slab_t *slab = cache.slabs[i];

        if (slab->header.size == size && slab->header.free && slab->header.used < PAGE_SIZE * PAGE_NUM / slab->header.size)
        {
            slab->header.used++;
            void *ptr = slab->header.free;
            slab->header.free = (char *)slab->header.free + size;
            return ptr;
        }
    }

    slab_t *slab = alloc_slab(size);
    if (!slab)
        return NULL;

    slab->header.used = 1;
    slab->header.free = slab->data + size;

    if (cache.count < CACHE_SIZE)
    {
        cache.slabs[cache.count++] = slab;
    }
    else
    {
        int victim_index = -1;
        size_t min_used = (size_t)-1;
        for (int i = 0; i < cache.count; i++)
        {
            slab_t *curr_slab = cache.slabs[i];
            if (curr_slab->header.used < min_used)
            {
                victim_index = i;
                min_used = curr_slab->header.used;
            }
        }

        if (victim_index == -1)
        {
            return NULL;
        }

        free_slab(cache.slabs[victim_index]);
        cache.slabs[victim_index] = slab;
    }

    return slab->data;
}

void mem_free(void *ptr)
{
    for (int i = 0; i < cache.count; i++)
    {
        slab_t *slab = cache.slabs[i];

        if (ptr >= (void *)slab->data && ptr < (void *)(slab->data + PAGE_NUM * PAGE_SIZE))
        {
            slab->header.used--;

            if (slab->header.used == 0)
            {
                for (int j = i; j < cache.count - 1; j++)
                {
                    cache.slabs[j] = cache.slabs[j + 1];
                }
                cache.count--;

                free_slab(slab);
            }
            return;
        }
    }
}

void *mem_realloc(void *ptr, size_t new_size)
{
    for (int i = 0; i < cache.count; i++)
    {
        slab_t *slab = cache.slabs[i];

        if (ptr >= (void *)slab->data && ptr < (void *)(slab->data + PAGE_NUM * PAGE_SIZE))
        {
            size_t current_size = slab->header.size;

            if (new_size <= current_size)
                return ptr;

            if (new_size > MAX_OBJ_SIZE)
            {
                return ptr;
            }

            void *new_ptr = mem_alloc(new_size);
            if (!new_ptr)
            {
                return ptr;
            }

            memcpy(new_ptr, ptr, current_size);

            mem_free(ptr);

            return new_ptr;
        }
    }

    return NULL;
}

void mem_show(const char *message)
{
    printf("\n%s", message);

    printf("\n-----------------------------------------------------------------------------------\n");
    printf("| %-9s| %-15s| %-12s| %-8s| %-11s| %-16s |\n", "Slab №", "Slab Address", "Object Size", "Usage", "Used/Total", "Object Addresses");
    printf("-----------------------------------------------------------------------------------\n");

    for (int i = 0; i < cache.count; i++)
    {
        slab_t *slab = cache.slabs[i];
        const char *usage = slab->header.used == 0 ? "empty" : (slab->header.used == PAGE_SIZE * PAGE_NUM / slab->header.size ? "full" : "partial");

        printf("| %-7d| %-15p| %-12zu| %-8s| %-1zu/%-9zu| ",
               i + 1, (void *)slab, slab->header.size, usage, slab->header.used, PAGE_SIZE * PAGE_NUM / slab->header.size);

        uintptr_t start_address = (uintptr_t)slab + sizeof(slab_header_t);
        for (size_t j = 0; j < slab->header.used; j++)
        {
            uintptr_t object_address = start_address + j * slab->header.size;
            printf("%p ", (void *)object_address);
        }

        printf("\n");
    }

    printf("-----------------------------------------------------------------------------------\n");
}
